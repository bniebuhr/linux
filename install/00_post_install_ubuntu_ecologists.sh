#!/bin/bash

## upgrade 
sudo apt update -y
sudo apt upgrade -y
sudo apt dist-upgrade -y

## repository

## qgis repo
#sudo add-apt-repository -y "deb https://qgis.org/ubuntu-nightly $(lsb_release -cs) main"
sudo add-apt-repository -y "deb https://qgis.org/ubuntu-ltr $(lsb_release -cs) main"

## public keys
wget -O - https://qgis.org/downloads/qgis-2017.gpg.key | gpg --import
gpg --fingerprint CAEB3DC3BDF7FB45
gpg --export --armor CAEB3DC3BDF7FB45 | sudo apt-key add -

## brave repo
sudo apt install curl apt-transport-https -y
curl -s https://brave-browser-apt-release.s3.brave.com/brave-core.asc | sudo apt-key --keyring /etc/apt/trusted.gpg.d/brave-browser-release.gpg add -
source /etc/os-release
echo "deb [arch=amd64] https://brave-browser-apt-release.s3.brave.com/ $UBUNTU_CODENAME main" | sudo tee /etc/apt/sources.list.d/brave-browser-release-${UBUNTU_CODENAME}.list

## etcher repo
echo "deb https://dl.bintray.com/resin-io/debian stable etcher" | sudo tee /etc/apt/sources.list.d/etcher.list
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 379CE192D401AB61

## skype repo
wget -q -O - https://repo.skype.com/data/SKYPE-GPG-KEY | sudo apt-key add -
echo "deb https://repo.skype.com/deb stable main" | sudo tee /etc/apt/sources.list.d/skypeforlinux.list

## sublime-text repo
wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | sudo apt-key add -
echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublime-text.list

## teamviewer repo
sudo sh -c "echo 'deb http://linux.teamviewer.com/deb stable main' >> /etc/apt/sources.list.d/teamviewer.list"
wget -q https://download.teamviewer.com/download/linux/signature/TeamViewer2017.asc -O- | sudo apt-key add -

## vlc repo
sudo add-apt-repository "deb http://archive.ubuntu.com/ubuntu $(lsb_release -sc) universe"

## update ppas
sudo apt update -y

## apps
sudo apt install -y\
 brave-keyring\
 brave-browser\
 catfish\
 default-jre\
 deluge\
 #etcher-electron\
 firefox\
 gdebi\ 
 gparted\ 
 grass\ 
 gimp\ 
 inkscape\ 
 kile\ 
 libcairo2-dev\
 libgdal-dev\
 libjpeg-dev\
 libgmp3-dev\
 libreoffice\
 libproj-dev\
 libssl-dev\
 libudunits2-dev\
 openjdk-11-jre-headless\
 python-qgis\
 qgis\
 qgis-plugin-grass\
 r-base\
 r-base-dev\
 r-cran-curl\
 r-cran-openssl\
 r-cran-rjava\
 r-cran-xml2\
 rhythmbox\
 saga\
 skypeforlinux\
 sublime-text\
 speedtest-cli\
 spyder3\
 teamviewer\
 #texlive-latex-extra\
 vlc\
 vlc-plugin-fluidsynth\
 vlc-plugin-vlsub

# rstudio
# wget https://download1.rstudio.org/rstudio-1.1.463-amd64.deb
# sudo dpkg -i rstudio-1.1.463-amd64.deb
# rm rstudio-1.1.463-amd64.deb
# it needs other dependencies - take a look at the other script

# gitkraken
wget https://release.gitkraken.com/linux/gitkraken-amd64.deb
sudo gdebi gitkraken-amd64.deb
rm gitkraken-amd64.deb

# foxit reader
# Download it manually from 
# https://www.foxitsoftware.com/pdf-reader/

# MasterPDF
wget https://code-industry.net/public/master-pdf-editor-5.2.20_qt5.amd64.deb
sudo dpkg -i master-pdf-editor-*.deb
rm master-pdf-editor-5.2.20_qt5.amd64.deb

# Mendeley Desktop
# Get the file manually
wget https://www.mendeley.com/repositories/ubuntu/stable/amd64/mendeleydesktop-latest
sudo gdebi mendeleydesktop*
rm mendeleydesktop*

# Bluetooth
sudo apt install -y blueman

# Google Earth
# Open http://www.google.com/earth/download/ge/agree.html and download Google Earth for Linux. Select the .deb package for your CPU architecture (32 or 64-bit). If you click on Advanced Setup you can choose the latest version of Google Earth or the previous version. Note to 64bit users: The 64bit debian package depends on ia32-libs which is deprecated and no longer available as of 13.10 Saucy. Use the 32bit package and multiarch-support. 
sudo apt install lsb-core -y
sudo gdebi ~/Downloads/google-earth-pro-stable_current_amd64.deb

## fix broken
sudo apt-get clean && sudo apt-get update
sudo dpkg --configure -a
sudo apt-get install -f
sudo apt install -y --fix-broken 

## cleanup apt
sudo apt autoremove -y
sudo apt autoclean -y
